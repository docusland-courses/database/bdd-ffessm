# BDD - FFESSM


La FFESSM (Fédération Française des Ecoles de Sports Sous-Marins) vous charge de modéliser une base de données destinée à observer l’activité des clubs de
plongée du littoral ainsi que l’expérience des moniteurs et des plongeurs. Les informations que la FFESSM souhaite modéliser sont résumées dans le compte rendu
d’enquête proposé ci-dessous.

Pour chaque club enregistré, la FFESSM conserve :
 - un numéro
 - un nom
 - une adresse
 - un numéro de téléphone. 
 

Pour assurer les plongées, les clubs embauchent des moniteurs qui doivent être diplômés par la FFESSM. Ce diplôme est sanctionné par un numéro de moniteur et un grade. Dans le cadre de cette étude, la FFESSM souhaite connaître l’historique de tous les clubs dans lesquels un moniteur a été embauché, avec les dates de début et de fin d’embauche. 

Quelques informations personnelles concernant les moniteurs sont aussi nécessaires. Il s’agit de:
 - nom,
 - prénom, 
 - adresse, 
 - téléphone, 
 - date de naissance 
 - groupe sanguin du moniteur. 
 
Enfin, il est important de connaître le président de chaque club, qui doit être un moniteur diplômé. Il n’est pas nécessaire de conserver l’historique pour les présidents de club.
Chaque plongeur est identifié à l’aide d’un passeport, sur lequel figurent ses nom, prénom, adresse, date de naissance, groupe sanguin et niveau. Afin de pouvoir
plonger, le plongeur doit avoir une licence en cours de validité, qui lui tient lieu d’assurance. Les licences sont délivrées par les clubs. Une licence est valable un an à partir de sa date de délivrance. 

Sur une licence, figurent :
 - un numéro de licence, 
- les nom, prénom, adresse et date de naissance du plongeur, 
- la date de délivrance 
- le numéro du club ayant délivré la licence.

Pour observer l’activité des clubs ainsi que l’expérience des moniteurs et des plongeurs, la FFESSM enregistre toutes les palanquées organisées par les clubs. Une palanquée est un groupe de plongeurs effectuant une plongée ensemble, sous la conduite d’un chef de palanquée. Le chef de palanquée est un moniteur. Pour chaque palanquée, il est nécessaire d’enregistrer la date, l’heure, le site, la profondeur, la durée, le club organisateur, le chef de palanquée ainsi que la liste des plongeurs y participant.

## Livrable attendu
 - Dictionnaire de données
 - Modèle Entité Association
 - Modèle Phyique de Données